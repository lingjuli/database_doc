package com.dhj.entity;

import lombok.Data;

/**
 * @Author gmd
 * @Description 字段名称和字段类型整合类
 * @Date 2023-08-04 18:38:28
 */
@Data
public class KeyTypeEntity {

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 字段类型
     */
    private String typeName;

    public KeyTypeEntity(String fieldName, String typeName) {
        this.fieldName = fieldName;
        // 字段类型首字母转小写
        this.typeName = Character.toLowerCase(typeName.charAt(0)) + typeName.substring(1);
    }

}
