package com.dhj.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.IoUtil;
import com.dhj.core.DealWordHandler;
import com.dhj.entity.StyleEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URLEncoder;
import java.util.Date;

import static com.dhj.constants.ConfigConstants.*;

/**
 * @Author gmd
 * @Description word下载controller层
 * @Date 2022-09-21 17:20:09
 */
@RestController
@RequestMapping("/download")
@RequiredArgsConstructor
public class DownloadController {

    private final DealWordHandler dealWordHandler;

    @Value("${project.name:XXX项目}")
    private String projectName;


    /**
     * 下载MySQL word，文件自动由后台保存到指定位置
     */
    @GetMapping("/mysql")
    public String mysql() {
        return saveWord(MYSQL);
    }

    /**
     * 下载MongoDB word，文件自动由后台保存到指定位置
     */
    @GetMapping("/mongo")
    public String mongo() {
        return saveWord(MONGO);
    }

    /**
     * 保存文档
     */
    private String saveWord(String database) {
        String fileName = projectName + "数据库设计文档_" + DateUtil.format(new Date(), "HHmmss");
        try {
            dealWordHandler.createWordFile(StyleEntity.getDefaultStyle(), FILE_SAVE_PATH + fileName + SUFFIX, database);

        } catch (Exception e) {
            e.printStackTrace();
            return "<h1>word文件生成错误！" + e.toString() + "</h1>";
        }
        return "<h1>word文件生成成功！文件名称：" + fileName + SUFFIX + "</h1>";
    }


    /**
     * 下载MySQL word，返回文件流，通过浏览器下载文件
     */
    @GetMapping("/getMysql")
    public void getMysql(HttpServletResponse response) {
        getWordStream(response, MYSQL);
    }

    /**
     * 下载Mongo word，返回文件流，通过浏览器下载文件
     */
    @GetMapping("/getMongo")
    public void getMongo(HttpServletResponse response) {
        getWordStream(response, MONGO);
    }

    public void getWordStream(HttpServletResponse response, String database) {
        OutputStreamWriter utf8Writer = null;
        try {
            String fileName = projectName + "数据库设计文档_" + DateUtil.format(new Date(), "HHmmss");

            // 通知浏览器以附件的形式下载处理，设置返回头要注意文件名有中文
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8") + SUFFIX);

            utf8Writer = IoUtil.getUtf8Writer(response.getOutputStream());

            // 写入文件数据
            dealWordHandler.writeWordData(utf8Writer, database);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 优雅关闭
            try {
                utf8Writer.flush();
                utf8Writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
