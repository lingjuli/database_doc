package com.dhj.service.mysql.impl;

import com.dhj.mapper.TableInfoMapper;
import com.dhj.service.mysql.ITableService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author gmd
 * @Description 表信息service实现层
 * @Date 2022-09-22 15:43:22
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TableServiceImpl implements ITableService {

    private final TableInfoMapper tableInfoMapper;

    /**
     * 需要导出的MySQL数据库名称，如果没有则默认test数据库
     */
    @Value("${project.database-name:test}")
    private String databaseName;

    @Override
    public List<Map<String, String>> queryAllTableName() {
        return tableInfoMapper.queryAllTableName(databaseName);
    }

    @Override
    public List<Map<String, String>> queryTableInfoByName(String tableName) {
        return tableInfoMapper.queryTableInfoByName(databaseName, tableName);
    }

}
