package com.dhj.service.mysql;

import java.util.List;
import java.util.Map;

/**
 * @Author gmd
 * @Description 表信息service层
 * @Date 2022-09-22 15:42:39
 */
public interface ITableService {

    /**
     * 获取指定数据库下所有的表
     */
    List<Map<String, String>> queryAllTableName();

    /**
     * 根据表名称获取表的属性信息
     */
    List<Map<String, String>> queryTableInfoByName(String tableName);

}
