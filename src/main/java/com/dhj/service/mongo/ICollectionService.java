package com.dhj.service.mongo;

import java.util.List;
import java.util.Map;

/**
 * @Author gmd
 * @Description 集合信息service层
 * @Date 2023-08-04 18:39:40
 */
public interface ICollectionService {

    /**
     * 获取指定数据库下所有的集合
     */
    List<Map<String, String>> queryAllCollectName();

    /**
     * 根据集合名称获取集合的属性信息
     */
    List<Map<String, String>> queryCollectInfoByName(String tableName);

}
