package com.dhj.constants;

/**
 * @Author gmd
 * @Description 系统配置信息
 * @Date 2022-09-22 15:38:58
 */
public class ConfigConstants {

    /**
     * 文件保存路径
     */
    public final static String FILE_SAVE_PATH = "./src/main/resources/static/word/";

    /**
     * 文件后缀名
     */
    public final static String SUFFIX = ".doc";

    /**
     * word常规布局下，表单的最大宽度
     */
    public final static Integer MAX_WIDTH = 8520;

    /**
     * MySQL和MongoDB数据库区分
     */
    public final static String MYSQL = "mysql";
    public final static String MONGO = "mongo";

    /**
     * MongoDB子元素前缀
     */
    public final static String SUB_ELE_PREFIX = "∟";


}
