package com.dhj;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author gmd
 * @Description 主程序启动类
 * @Date 2022-09-21 16:53:09
 */
@MapperScan("com.dhj.mapper")
@SpringBootApplication
public class DatabaseDocApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatabaseDocApplication.class, args);
    }

}
