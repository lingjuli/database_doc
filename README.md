# 一、前言
在日常的开发工作中，偶尔会遇到项目收尾时，需要后端开发人员提供数据库设计文档的情况。  
我把相关的文档规范、排版样式等，整理成了博客，方便你快速地知道如何编写一篇数据库设计文档。  
参考文章链接： https://blog.csdn.net/qq_41057885/article/details/114436170  
然而，即使你知道如何编写数据库设计文档，它的编写工作依然十分繁琐、重复。在浪费了大量的时间在此事后，我决定编写一个程序，把我从繁琐、重复的工作中解放出来。

## 1.1 项目目标
通过程序连接数据库（MySQL、MongoDB），个性化生成数据库设计文档。

## 1.2 项目说明
本项目的代码逻辑是通过对word文档标签进行操作，来达到修改word内容的目的，然后再将数据表的相关信息动态插入其中。  
因此，本项目的重点就在于对word标签的操作，只有理解了word标签，才能理解本项目的代码逻辑。


# 二、MongoDB导出说明

## 2.1 关于MongoDB集合字段导出说明
MongoDB集合的字段是不固定，其导出可以有两种方案。  
1.导出集合最大公约数字段集。  
2.导出集合中所有出现过的字段。  
对于超大数据集（生产环境）来说，遍历以获取所有的字段的方案是不现实的。在不遍历的前提下，不管是哪种方案，都无法获取最真实的字段集，都只能尽可能接近真实值。  
本程序为了方便只是获取集合的第一条数据来获取字段名称及字段类型。
Tip：这是一个数学问题，即如何从一个大集合中筛选出一个小集合，且使小集合能尽可能多地含有大集合的特征。（盼望大佬们赐教）

## 2.2 导出文档的留白
此外，由于非关系型数据库的特点，MongoDB导出的文档就会有很多留白。实际上，这里的导出也就帮助用户整理好了格式，填充了字段名称、字段类型以及一些常规的信息，其他的均需要用户自己修改或补充。

## 2.3 mongoDB字段的父子（包含）关系
为了在导出的word中体现字段间的关系，需要通过一些方式来体现。   
对于同级关系，两个字段左对齐显示。
对于包含关系，则给子字段加一个前缀“∟”，子字段的子字段则加两个前缀“∟”，例如：“∟∟name”，依此类推。

## 2.4 MongoDB中的数据类型
MongoDB字段数据类型对应Java的数据类型有如下：
<img src="src/main/resources/static/images/6.png" alt="yml配置" width="500">  
java.util.ArrayList  
java.lang.Boolean  
org.bson.types.CodeWithScope  
java.util.Date  
org.bson.types.Decimal128  
java.lang.Double  
java.lang.Integer  
java.lang.Long  
org.bson.types.MaxKey  
org.bson.types.MinKey  
null  
java.util.LinkedHashMap  
java.lang.String  
org.bson.types.Symbol  
org.bson.BsonTimestamp  
org.bson.types.ObjectId  


# 三、使用说明

## 2.1 下载默认配置
#### 1、替换数据库，启动项目
找到application.yml配置文件，修改如图所示的配置  
MySQL数据库配置：
<img src="src/main/resources/static/images/1.jpg" alt="yml配置" width="800">
MongoDB数据库配置：
<img src="src/main/resources/static/images/7.png" alt="yml配置" width="800">

#### 2、启动项目
项目默认端口为8888

#### 3、浏览器访问接口

| 接口地址                                       | 说明                             |
|-----------------------------------------------|--------------------------------|
| http://127.0.0.1:8888/download/mysql          | 下载MySQL的word到本地                |
| http://127.0.0.1:8888/download/mongo          | 下载MongoDB的word到本地              |
| http://127.0.0.1:8888/download/getMysql       | 返回MySQL文件流，浏览器直接下载             |
| http://127.0.0.1:8888/download/getMongo       | 返回MongoDB文件流，浏览器直接下载           |

#### 4、补充
word默认的下载地址为：./src/main/resources/static/word/
注意：以上4种方式下载的文件，都是采用默认格式，默认字段（序号、字段名、类型(长度)、主键、描述）。
如果你需要更多的字段和其他样式，可以使用***自定义配置页面***


## 2.2 自定义配置页面
项目启动后，浏览器访问 http://127.0.0.1:8888/form/index 即可进入配置页面，选择你需要的配置（表头字段、表格样式）即可下载相应的word文档。  
多选框后面的输入框为字段别名，例如当你选中了第一行（序号），但你希望生成的文档叫序列号，即可在后面的输入框输入序列号即可。  
<img src="src/main/resources/static/images/2.png" alt="自定义配置页面" width="700">
#### 效果
<img src="src/main/resources/static/images/3.png" alt="修改自定义配置" width="600">  
<img src="src/main/resources/static/images/4.png" alt="下载成功" width="600">  
<img src="src/main/resources/static/images/5.png" alt="效果页面" width="600">  

# 四、视频教程
B站视频地址：
【database_doc项目的用法及实现逻辑说明】 https://www.bilibili.com/video/BV1MM411K7ko


# 五、其他
本项目补充了MongoDB的数据库导出，部分页面有改动，不过问题不大。  
如果你有任何好的意见或建议，可以联系我（如果我还做程序员的话🤡️）。  
🖐telephone / wx：18370815482
